#!/bin/sh

if [ ! -d "~/.oh-my-zsh" ]; then
    echo Installing oh-my-zsh
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
fi


ln -s -i ~/git/initFiles/ssh-config ~/.ssh/config
# ln -s -i -T ~/git/initFiles/.zsh/ ~/.zsh
# ln -s -i ~/git/initFiles/.zshrc ~/.zshrc
ln -s -i ~/git/initFiles/zshrc.zsh ~/.oh-my-zsh/custom/zshrc.zsh
ln -s -i ~/git/initFiles/.zshenv ~/.zshenv
# ln -s -i ~/git/initFiles/.bashrc ~/.bashrc
ln -s -i ~/git/initFiles/.screenrc ~/.screenrc
ln -s -i ~/git/initFiles/.gitconfig ~/.gitconfig
ln -s -i ~/git/initFiles/.pylintrc ~/.pylintrc
ln -s -i ~/git/initFiles/.pyflymakerc ~/.pyflymakerc
ln -s -i ~/git/initFiles/.tmux.conf ~/.tmux.conf

ln -s -i -T ~/git/initFiles/terminator ~/.config/terminator

if [ "$HOST" = "beryl" ]; then
    sudo ln -s -i ~/git/initFiles/ARM-config /opt/arm/config
fi
