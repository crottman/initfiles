platform='unknown'
unamestr=`uname`
if [[ "$unamestr" == 'Linux' ]]; then
    platform='linux'
elif [[ "$unamestr" == 'Darwin' ]]; then
    platform='osx'
fi

# export EDITOR="emacs -nw"
export EDITOR="emacsclient -t"
alias .='cd .'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias .......='cd ../../../../../..'

alias sl='ls --color=auto'
alias sls='ls --color=auto'
alias ls='ls --color=auto'
alias l='ls -al --color=auto'
alias got='ps ax|grep -v grep|grep'

alias notes='emacs -nw ~/Dropbox/orgnotes/notes.org'
alias Notes='emacs ~/Dropbox/orgnotes/notes.org'
alias notes-org='emacs -nw ~/Dropbox/orgnotes/notes/org-notes.org'
alias Notes-org='emacs ~/Dropbox/orgnotes/notes/org-notes.org'
alias notes-emacs-config='emacs -nw ~/.emacs.d/config.org'
alias Notes-emacs-config='emacs ~/.emacs.d/config.org'
alias notes-ubuntu='emacs -nw ~/Dropbox/orgnotes/ubuntu.org'
alias Notes-ubuntu='emacs ~/Dropbox/orgnotes/ubuntu.org'
alias diskspace='df -h'
alias foldersize='du -hs ./*'

alias -g SIL='&>/dev/null&;disown'
alias -g SILL='&>/dev/null&;disown && exit'
alias todo='emacs -nw ~/Dropbox/orgnotes/todo.org'

alias -g SIL='&>/dev/null&;disown'
alias -g SILL='&>/dev/null&;disown && exit'
# alias checkgit='~/.zsh/checkgit.py'
# alias please='sudo !!'          # doesn't work...

alias please='sudo $(fc -ln -1)'

if [ "${HOST}" = "beryl" ]
then
    alias handbrake='ghb'
    alias startvnc='vncserver -geometry 1840x1040 :1'
    alias stopvnc='vncserver -kill :1'
else
fi

if [[ "$platform" == 'osx' ]]; then
    alias sl='ls -G'
    alias sls='ls -G'
    alias ls='ls -G'
    alias l='ls -alG'
    alias emacs='/Applications/MacPorts/Emacs.app/Contents/MacOS/Emacs'
    # alias emacsclient='/Applications/MacPorts/Emacs.app/Contents/MacOS/bin/emacsclient'
    # alias emacsclient="which osascript > /dev/null 2>&1 && osascript -e 'tell application Emacs to activate' && emacsclient -c '$@'"
    # export EDITOR="emacs -nw"
    # export EDITOR="/Applications/MacPorts/Emacs.app/Contents/MacOS/bin/emacsclient -nw"
    export EDITOR="/Applications/MacPorts/Emacs.app/Contents/MacOS/Emacs -nw" # emacsclient is weird on osx

    eval `ssh-agent`
    ssh-add
    export PATH=$PATH:"/opt/local/bin:/opt/local/sbin:$PATH"
    alias runsnake='runsnake32'

    export LESSOPEN="| /usr/local/bin/src-hilite-lesspipe.sh %s"
else
    export LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s"
    export LESS=' -R '
fi
PATH=$PATH:~/.cabal/bin:~/git/initFiles/bin/:~/.emacs.d/bin/


# Attempt at setting DBUS
if [ -n "$SSH_CLIENT" -a -n "$DISPLAY" ]; then
    machine_id=$(LANGUAGE=C hostnamectl|grep 'Machine ID:'| sed 's/^.*: //')
    x_display=$(echo $DISPLAY|sed 's/^.*:\([0-9]\+\)\(\.[0-9]\+\)*$/\1/')
    dbus_session_file="$HOME/.dbus/session-bus/${machine_id}-${x_display}"
    if [ -r "$dbus_session_file" ]; then
        export $(grep '^DBUS.*=' "$dbus_session_file")
        # check if PID still running, if not launch dbus
        ps $DBUS_SESSION_BUS_PID | tail -1 | grep dbus-daemon >& /dev/null
        [ "$?" != "0" ] && export $(dbus-launch) >& /dev/null
    else
        export $(dbus-launch) >& /dev/null
    fi
fi

p() {
    if (( $# == 1 ))
    then ccat ~/Dropbox/passwords.cpt | grep -i $1
    elif (( $# == 2 ))
    then ccat ~/Dropbox/passwords.cpt | grep -i $1 | grep -i $2
    elif (( $# == 3 ))
    then ccat ~/Dropbox/passwords.cpt | grep -i $1 | grep -i $2 | grep -i $3
    fi
}

wcp(){
    # Copies a file into my usr/sci/www folder and then echos/copies the link
    # Does not allow changing the file name
    # A second argument tells what folder in my www folder to place the file
    if (($# == 1))
    then
        cp -r $1 /usr/sci/www/crottman/
        echo www.sci.utah.edu/~crottman/$(basename $1)
        echo www.sci.utah.edu/~crottman/$(basename $1) | xclip
    fi
    if (($# == 2))
    then
        webdir="$2"
        i=$((${#webdir}-1))
        if [ "${webdir:$i:1}" = "/" ] # if last character is a slash
        then
            webdir="${webdir%?}" # remove last character
        fi
        cp -r $1 /usr/sci/www/crottman/$webdir/
        echo www.sci.utah.edu/~crottman/$webdir/$(basename $1)
        echo www.sci.utah.edu/~crottman/$webdir/$(basename $1) | xclip
    fi
}

# emacsclient stuff
export ALTERNATE_EDITOR=""
alias et='emacsclient -t'
alias em='emacsclient -c'
alias e='emacs -nw'

# look better for emacs -nw, and work for tmux
if [ $TMUX ]
then
    TERM=screen-256color            # works for tmux, but less/ccmake suck
else
    TERM=xterm-256color
fi


# get rid of stupid GUI - opensuse only?
export SSH_ASKPASS=

export BORG_PASSCOMMAND="cat /home/crottman/.borg-passphrase"
# source ~/.zsh/git-prompt/zshrc.sh

# ZSH_THEME=~/.zsh/agnoster.zsh-theme
ZSH_THEME="agnoster"
# source ~/git/zsh-git-prompt/zshrc.sh

# don't know what this does
autoload -Uz compinit
compinit

#allow tab completion in the middle of a word
setopt COMPLETE_IN_WORD

# Better Arrow Up/Down
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
# bindkey "^[[A" up-line-or-beginning-search # Up
# bindkey "^[[B" down-line-or-beginning-search # Down
bindkey "\C-p" up-line-or-beginning-search # Up
bindkey "\C-n" down-line-or-beginning-search # Down

# an example prompt for git super status
# PROMPT='%B%m%~%b$(git_super_status) %# '
# PROMPT='%{$fg_bold[green]%}%B%m%{$fg_bold[white]%}:%{$fg_bold[blue]%}%~%b$(git_super_status) %{$fg_bold[white]%}%#%{$reset_color%} '

# don't use reset_color before git_super_status

# %m = hostname
# %1~ = only show current folder (ignoring /home/...)
# %~ = show current directory (ignoring /home/...)
# %{$fg_bold[green]%} = switch color to green
# %{$reset_color%} = self explanatory


# # Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000
# setopt appendhistory beep extended_glob nomatch notify
# bindkey -e
# # End of lines configured by zsh-newuser-install
# # The following lines were added by compinstall
# zstyle :compinstall filename '/home/jacob/.zshrc'

# autoload -U colors zsh/terminfo # Used in the colour alias below
# colors
# setopt prompt_subst

# 	# make some aliases for the colours: (coud use normal escap.seq's too)
# for color in RED GREEN YELLOW BLUE MAGENTA CYAN WHITE; do
# 		eval PR_$color='%{$fg[${(L)color}]%}'
#     eval PR_BRIGHT_$color='%{$fg_bold[${(L)color}]%}'
# done
# PR_RESET="%{${reset_color}%}";

# # Git indicators for prompt
# source ~/.zsh/git-prompt/zshrc.sh


# # colors for ls
# LS_COLORS='rs=0:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:tw=30;42:ow=34;42:st=37;44:ex=01;32:';
# export LS_COLORS

# # completion stuff
# zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

# zstyle ':completion:*:*:kill:*' menu yes select
# zstyle ':completion:*:kill:*'   force-list always

# zstyle ':completion:*:*:killall:*' menu yes select
# zstyle ':completion:*:killall:*'   force-list always

# # arrow-key completion
# zstyle ':completion:*' menu select

# # command L equivalent to command |less
# #alias -g L='|less'

# # format all messages not formatted in bold prefixed with ----
# zstyle ':completion:*' format '%B---- %d%b'
# # format descriptions (notice the vt100 escapes)
# zstyle ':completion:*:descriptions'    format $'%{\e[0;31m%}completing %B%d%b%{\e[0m%}'
# # bold and underline normal messages
# zstyle ':completion:*:messages' format '%B%U---- %d%u%b'
# # format in bold red error messages
# zstyle ':completion:*:warnings' format "%B$fg[red]%}---- no match for: $fg[white]%d%b"

# # group completions by type
# zstyle ':completion:*' group-name ''

# # This colors all STDERR output as red JDH- This has the problem that
# # if STDERR does not include a newline then nothing gets printed until
# # a newline is printed, even if the output has been flushed... not
# # good.
# #exec 2>>(while read line; do print '\e[91m'${(q)line}'\e[0m' > /dev/tty; print -n $'\0'; done &)

# # let's complete known hosts and hosts from ssh's known_hosts file
# basehost=""
# hosts=($((
# ( [ -r .ssh/known_hosts ] && awk '{print $1}' .ssh/known_hosts | tr , '\n'); echo $basehost; ) | sort -u) )

# zstyle ':completion:*' hosts $hosts

# #
# # leftover stuff from my bashrc
# #

# alias src=src-hilite-lesspipe.sh
alias diff="colordiff"
# # use this to force color even if piping into less
#alias grepc="grep --color=always"
alias grep="grep --color=auto"
# alias df="$HOME/src/script/pydf || df"
# alias ls="ls -hF --color=always"
# #alias l="ls -al"
# # the following is so that less can interpret color sequences correctly
# alias less="less -R"


# oh my zsh customizations
ZSH_THEME="agnoster"
CASE_SENSITIVE="true"

alias check_backup='cat /home/crottman/Dropbox/scripts/backups/logs/borg.log /home/crottman/Dropbox/scripts/backups/logs/rclone.log | less'
alias backupstatus='/home/crottman/Dropbox/scripts/backups/backupstatus.sh|less'

if [[ "$platform" == 'osx' ]];
then
    # export DEFAULT_USER=calebrottman
    export DEFAULT_USER=crottman
else
    export DEFAULT_USER=crottman
fi
