#!/bin/sh

read -p "Enter text to be replaced (no spaces): " regin
read -p "Enter text to replace it (no spaces): " replace

# echo "here is the regex: " $regin


# while true; do
#     read -p "Should this be case sensitive? y/n " yn
#     case $yn in
#         [Yy]* ) echo y ; break ;;
#         [Nn]* ) echo n ; break ;;
#         * ) echo "Please answer yes or no.";;
#     esac
# done

# for filename in *$regin*; do
for filename in *"${regin}"*; do
    # echo mv -i \'"$filename"\' \'"`echo $filename | sed s/${regin}/${replace}/`"\';
    mv -i "$filename" "`echo $filename | sed s/${regin}/${replace}/`";
done


# rename $regin $replace *
