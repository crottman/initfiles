# If not running interactively, don't do anything
[[ "$-" != *i* ]] && return

export EDITOR="emacs -nw"
alias .='cd .'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias .......='cd ../../../../../..'

alias home='cd /home/sci/crottman/'
alias www='cd /usr/sci/www/crottman/'
# alias -g SIL='&>/dev/null&;disown'
alias checkgit='~/.zsh/checkgit.py'
alias e='emacs -nw'
alias todo='emacs -nw ~/Dropbox/todo.org'
p() {
    if (( $# == 1 ))
    then ccat ~/Dropbox/passwords.cpt | grep -i $1
    fi
    if (( $# == 2 ))
    then ccat ~/Dropbox/passwords.cpt | grep -i $1 | grep -i $2
    fi
    if (( $# == 3 ))
    then ccat ~/Dropbox/passwords.cpt | grep -i $1 | grep -i $2 | grep -i $3
    fi
}

alias ssht='ssh crottman@topaz.sci.utah.edu'
