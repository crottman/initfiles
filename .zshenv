# #-----------
# # PATH STUFF
# #-----------

# # Note: do not uncomment! Especially PATH and LD_LIBRARY_PATH
# # export PATH=
# # export PYTHONPATH=
# # export LD_LIBRARY_PATH=

# # Machine Specific Paths

# if [ "${HOST}" = "topaz" ]
# then
#     export TEXINPUTS=$TEXINPUTS:.:~/Notes/style-files/:~/git/myNotes/:~/Documents/info-dissertation/new-2016-05-03/bibtex/
#     export BSTINPUTS=$BSTINPUTS:.:~/Documents/info-dissertation/new-2016-05-03/bibtex
#     export PYTHONPATH=$PYTHONPATH:/home/sci/crottman/utrecon/build/lib.linux-x86_64-2.7:~/git/PythonCode/SlidingSeg:~/3DRecon/ConebeamRecon/:~/3DRecon/ConebeamProjection
#     export PATH=$PATH:/usr/local/crashplan/bin:/opt/kde3/bin
# elif [ "${HOST}" = "agate" ]
# then
#     # export PATH=$PATH:/usr/local/cuda-8.0/bin # just cuda should be fine i think
#     export PYTHONPATH=$PYTHONPATH:~/git/ConebeamRecon:~/git/ConebeamProjection
# elif [ "${HOST}" = "beryl" ]
# then
#     # export PYTHONPATH=$PYTHONPATH:~/software/pyca-bin/python_module
# else                            # other SCI machines including atlas
#     export TEXINPUTS=$TEXINPUTS:.:~/git/Notes/style-files/:~/git/myNotes/
#     export PYTHONPATH=$PYTHONPATH:~/3DRecon/ConebeamRecon/:~/3DRecon/ConebeamProjection
#     # export PATH=$PATH:~/software/bin:/usr/local/cuda/bin
# fi

# # Common Path stuff
# export GOPATH=$GOPATH:/local/crottman/software/go

# # common pythonpath and path
# # note: python-module exists on most computers, but doesn't exist on /home/sci/software, so this line is safe
# export PYTHONPATH=$PYTHONPATH:~/git/PyCACalebExtras:~/git/AppUtils:~/git/PyCAApps:~/software/pyca-bin/python_module
# # note: all computers with CUDA should have a /usr/local/cuda/bin that is soft-linked to the real cuda folder
export PATH=/usr/local/go/bin:$PATH:~/.cabal/bin:~/git/initFiles/bin/:~/.emacs.d/bin/:~/.local/bin:~/software/bin:/usr/local/cuda/bin:~/Dropbox/linux/scripts
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib64 # for MPI?
